let dynamicCharts = {};
let counties = {};
let defaultLineChartOptions = {
  maintainAspectRatio: false,
  scales: {
    yAxes: [
      {
        ticks: {
          beginAtZero: false,
        },
      },
    ],
  },
  plugins: {
    legend: {
      display: false,
    },
  },
};

function buildCharts(numberOfDays) {
  $('#loadingModal').modal('show'); 
  //Build state cases and death charts
  $.getJSON('https://disease.sh/v3/covid-19/nyt/states/tennessee?lastdays=' + numberOfDays, function (result) {
    let statsLabels = [];
    let casesData = [];
    let deathsData = [];
    $.each(result, function () {
      statsLabels.push(this.date);
      casesData.push(this.cases);
      deathsData.push(this.deaths);
    });
    let data = {
      labels: statsLabels,
      datasets: [
        {
          label: 'Total Cases',
          data: casesData,
          fill: true,
          borderColor: '#f28500',
          tension: 0.5,
        },
      ],
    };
    addDynamicChart('stateCasesChart', 'casesChart', 'line', data, defaultLineChartOptions);
    data = {
      labels: statsLabels,
      datasets: [
        {
          label: 'Total Deaths',
          data: deathsData,
          fill: true,
          borderColor: '#ff0000',
          tension: 0.5,
        },
      ],
    };
    addDynamicChart('stateDeathsChart', 'deathsChart', 'line', data, defaultLineChartOptions);
  });

  //Build the state vaccination percentage chart
  $.getJSON('https://data.cdc.gov/resource/unsk-b7fc.json', function (result) {
    let fullyVaccinated;
    $.each(result, function () {
      if (this.location == 'TN') {
        fullyVaccinated = this.series_complete_pop_pct;
      }
    });
    let unvaccinated = 100 - fullyVaccinated;
    let data = {
      labels: ['Vaccinated', 'Unvaccinated'],
      datasets: [
        {
          label: 'Vaccination Percentage',
          data: [fullyVaccinated, unvaccinated],
          backgroundColor: ['#9acd32', '#ce2029'],
          hoverOffset: 4,
        },
      ],
    };
    let options = {
      maintainAspectRatio: false,
      tooltips: {
        backgroundColor: 'rgb(255,255,255)',
        bodyFontColor: '#858796',
        borderColor: '#dddfeb',
        borderWidth: 1,
        xPadding: 15,
        yPadding: 15,
        displayColors: false,
        caretPadding: 10,
      },
      legend: {
        display: false,
      },
      cutoutPercentage: 70,
    };
    addDynamicChart('fullyVaccinatedPercentageChart', 'fullyVaccinatedPercentageChartArea', 'doughnut', data, options);
  });

  //Build the state vaccine dosages chart
  $.getJSON('https://disease.sh/v3/covid-19/vaccine/coverage/states/tennessee?lastdays=' + numberOfDays + '&fullData=false', function (result) {
    let vaccineLabels = [];
    let vaccinations = [];
    $.each(result.timeline, function (key, val) {
      vaccineLabels.push(key);
      vaccinations.push(val);
    });
    let data = {
      labels: vaccineLabels,
      datasets: [
        {
          label: 'Vaccinations',
          data: vaccinations,
          fill: true,
          borderColor: '#0073cf',
          backgroundColor: '#0073cf',
        },
      ],
    };
    let options = {
      maintainAspectRatio: false,
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: false,
            },
          },
        ],
      },
      plugins: {
        legend: {
          display: false,
        },
      },
    };
    addDynamicChart('vaccineDosagesChart', 'vaccineDosagesChartArea', 'bar', data, options);
  });

  //Pull counties data and build all county based charts
  //Populate the counties object for later chart use
  $('#countyCards').html('');
  $.getJSON('https://disease.sh/v3/covid-19/historical/usacounties/tennessee?lastdays=' + numberOfDays, function (result) {
    counties = {};
    let dates = Object.keys(result[0].timeline.cases);
    let countiesTableDate = dates[dates.length - 1];
    $.each(result, function () {
      let countyLabels = [];
      let countyCases = [];
      let countyDeaths = [];
      let countyName = capitalize(this.county);
      $.each(this.timeline.cases, function (key, val) {
        countyLabels.push(key);
        countyCases.push(val);
      });
      $.each(this.timeline.deaths, function (key, val) {
        countyDeaths.push(val);
      });
      counties[countyName] = {
        name: countyName,
        labels: countyLabels,
        cases: countyCases,
        deaths: countyDeaths,
      };
    });
    $.each(counties, function () {
      addCountyCard(this.name);
    });
    generateCountyCharts();
    if (!$.fn.dataTable.isDataTable('#countiesTable')) {
      generateCountiesTable(counties, countiesTableDate);
    }
    populateCountySpotlightSelect();
    updateSpotlightCounty();
    setTimeout(function () {
      $('#loadingModal').modal('hide');      
    }, 1000);    
  });
  
}

function addDynamicChart(chartName, canvasName, type, data, options) {
  if (dynamicCharts[chartName]) {
    dynamicCharts[chartName].destroy();
  }
  dynamicCharts[chartName] = new Chart(document.getElementById(canvasName), {
    type: type,
    data: data,
    options: options,
  });
}

function safelyDestroyChart(chart) {
  if (chart != undefined) {
    chart.destroy();
  }
}

function generateCountyCharts() {
  let version = $('#countyChartRadioButtons input:radio:checked').val();
  $.each(counties, function () {
    if (version == 'cases') {
      data = {
        labels: this.labels,
        datasets: [
          {
            label: 'Cases',
            data: this.cases,
            fill: true,
            borderColor: '#0047ab',
            tension: 0.5,
          },
        ],
      };
    } else {
      data = {
        labels: this.labels,
        datasets: [
          {
            label: 'Deaths',
            data: this.deaths,
            fill: true,
            borderColor: '#ff0000',
            tension: 0.5,
          },
        ],
      };
    }
    addDynamicChart(this.name, this.name + 'ChartArea', 'line', data, defaultLineChartOptions);
  });
}

function populateCountySpotlightSelect() {
  $.each(counties, function () {
    $('#countySpotlightSelect').append($('<option>').val(this.name).text(this.name));
  });
}

function updateSpotlightCounty() {
  let countyName = $('#countySpotlightSelect').val();
  let version = $('#spotlightChartRadioButtons input:radio:checked').val();
  let data;
  $.each(counties, function (county) {
    if (this.name == countyName) {
      if (version == 'cases') {
        data = {
          labels: this.labels,
          datasets: [
            {
              label: 'Cases',
              data: this.cases,
              fill: true,
              borderColor: '#0047ab',
              tension: 0.5,
            },
          ],
        };
      } else {
        data = {
          labels: this.labels,
          datasets: [
            {
              label: 'Deaths',
              data: this.deaths,
              fill: true,
              borderColor: '#ff0000',
              tension: 0.5,
            },
          ],
        };
      }
      addDynamicChart('countySpotlightChart', 'countySpotlightChartArea', 'line', data, defaultLineChartOptions);
    }
  });
}

function addCountyCard(countyName) {
  $('#countyCards').html(
    $('#countyCards').html() +
      '<div class="col"><div class="card h-100"><h5 class="card-title p-2">' +
      countyName +
      ' County</h5><div class="card-body h-100"><canvas id="' +
      countyName +
      'ChartArea"></canvas></div><div class="card-footer"></div></div></div>'
  );
}

function generateCountiesTable(counties, countiesTableDate) {
  let countiesTableData = [];
  $.each(counties, function (index) {
    let countyTableData = {
      County: counties[index].name,
      Cases: counties[index].cases[this.cases.length - 1],
      Deaths: counties[index].deaths[this.deaths.length - 1],
    };
    countiesTableData.push(countyTableData);
  });
  let countiesTable = $('#countiesTable').dataTable({
    data: countiesTableData,
    columns: [{ data: 'County' }, { data: 'Cases' }, { data: 'Deaths' }],
    dom: 'frtip',
  });
  $('#countiesTableDate').text(countiesTableDate);
}

function capitalize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function addActionListeners() {
  $('#daysSelect').change(function () {
    buildCharts(this.value);
  });

  $('#countySpotlightSelect').change(function () {
    updateSpotlightCounty();
  });

  $('#spotlightChartRadioButtons :input').change(function () {
    updateSpotlightCounty();
  });

  $('#countyChartRadioButtons :input').change(function () {
    generateCountyCharts();
  });
}

$( document ).ready(function() {
  buildCharts('30');
  addActionListeners();
  setTimeout(function () {
  updateSpotlightCounty();
}, 1000);
});