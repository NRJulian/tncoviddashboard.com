# TNCovidDashboard.com

TNCovidDashboard.com is a simple dashboard that visualizes Tennessee COVID statistics. 


|Screenshot 1 |Screenshot 2  |Screenshot 3 |
--- | --- | ---
|![Screenshot](./screenshots/screenshot_1.png)|![Screenshot](./screenshots/screenshot_2.png)|![Screenshot](./screenshots/screenshot_3.png)|

TNCovidDashboard.com is designed to convey important information as quickly and simply as possible. The design is clean and clear to enable the data to be have the emphasis. A small selection of controls enable the user to select the age range of the data and whether to view total cases or deaths.

TNCovidDashboard.com is build on:
- [Bootstrap 5](https://getbootstrap.com/)
- [JQuery](https://jquery.com/)
- [Chart.js](https://www.chartjs.org/)
- [Datatables](https://datatables.net/)

Data is pulled from the [disease.sh](https://disease.sh/) and [CDC](https://data.cdc.gov) COVID APIs.

TNCovidDashboard.com is free and open-source software under the [MIT](https://choosealicense.com/licenses/mit/) license.